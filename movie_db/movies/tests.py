from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Movie


class MovieTestCase(APITestCase):
    def test_post(self):
        url = reverse('movies')
        response = self.client.post(url, data={'movie': 'Blade Runner 2049'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Movie.objects.count(), 1)
        self.assertEqual(Movie.objects.first().title, 'Blade Runner 2049')
