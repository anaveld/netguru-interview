from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=500)
    year = models.IntegerField()
    rated = models.CharField(max_length=10)
    released = models.DateField()
    runtime = models.CharField(max_length=10)
    genre = models.CharField(max_length=500)
    director = models.CharField(max_length=150)
    writer = models.CharField(max_length=500)
    actors = models.CharField(max_length=500)
    plot = models.CharField(max_length=1000)
    language = models.CharField(max_length=1000)
    country = models.CharField(max_length=300)
    awards = models.CharField(max_length=300)
    imdb_rating = models.FloatField()
    box_office = models.IntegerField()
    production = models.CharField(max_length=300)
    website = models.URLField()
