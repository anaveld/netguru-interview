from django.http.response import Http404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Movie
from .omdb_api import search_movie
from .serializers import MovieSerializer


class MovieView(APIView):

    def get(self, request):
        queryset = Movie.objects.all()
        order_by = request.query_params.get('order_by', None)
        order = request.query_params.get('order_by', 'asc')
        search = request.query_params.get('search', None)
        if search:
            queryset = queryset.filter(title__iexact=search)

        if order_by in ['title', 'year', 'imdb_rating']:
            if order == 'desc':
                order_by = '-{}'.format(order_by)

            queryset = queryset.order_by(order_by)

        return Response(MovieSerializer(queryset, many=True).data)

    def post(self, request):
        movie_title = request.data.get('movie', None)
        if not movie_title:
            raise Http404()

        movie_data = search_movie(movie_title)
        status = movie_data.pop('response', None)
        if not status or status == 'False':
            raise Http404()

        serializer = MovieSerializer(data=movie_data)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)
