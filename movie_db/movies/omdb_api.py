from datetime import datetime

import requests
from django.conf import settings


def search_movie(movie_title):
    r = requests.get(settings.OMDB_API_URL, params={
        'apikey': settings.OMDB_API_KEY,
        't': movie_title
    })
    output = {}
    for k, v in r.json().items():
        if k == 'Released':
            v = datetime.strptime(v, '%d %b %Y').date()

        if k == 'BoxOffice':
            k = 'box_office'
            v = int(v.strip('$').replace(',', ''))

        if k == 'imdbRating':
            k = 'imdb_rating'

        output[k.lower()] = v

    return output
