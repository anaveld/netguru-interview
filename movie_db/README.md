This is mostly basic stuff with a lot of area for improvements.
Especially data scheme is very simple - in more production-like environment all of subdata
should be extracted to external models (ex. actors, languages etc). In more complex project
there shouldn't be so much logic in views - rather extracted to some handlers.py or smth similar.
SQLite is only chosen for simplicity - postgreSQL should be used. Whole stuff should be dockerized
for future developers in more commercial project. Also there should more usage of in-built Django features
in views (viewSets maybe). I'm really not very excited about this task but tons of responsibilities during this week
discouraged me to spend more time on this task. Hope I will be able to explain in next step of interview :)

To run this project just install requirements in venv and run 'python manage.py runserver'

API endpoints are just as you have described. Param for movie name is 'movie'.
