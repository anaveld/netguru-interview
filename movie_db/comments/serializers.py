from rest_framework import serializers

from .models import Comment

from movies.models import Movie


class CommentSerializer(serializers.ModelSerializer):
    movie = serializers.PrimaryKeyRelatedField(queryset=Movie.objects.all())

    class Meta(object):
        model = Comment
        fields = (
            'movie',
            'text',
        )
