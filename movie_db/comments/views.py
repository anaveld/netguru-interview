from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Comment
from .serializers import CommentSerializer


class CommentView(APIView):

    def get(self, request):
        queryset = Comment.objects.all()
        movie_id = request.query_params.get('movie_id', None)
        if movie_id:
            queryset = queryset.filter(movie_id=movie_id)

        return Response(CommentSerializer(queryset, many=True).data)

    def post(self, request):
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)
